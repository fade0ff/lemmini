Lemmini is the attempt to combine the playing sensation of the original Amiga version of Lemmings with the improved graphics of the Win95 port to create the best possible Lemmings experience on all platforms that support a Java virtual machine.

See http://lemmini.de/

---------------------------------------------------------------
Copyright 2015-2017 Volker Oth - VolkerOth(at)gmx.de

All files that are my own creation are released under the Apache License 2.0. In a nutshell that means you can use them and modify them even for closed source or commercial projects, as long as you leave some copyright info in and give appropriate credit. See the linked Apache License for details.
Note that there are also some sources included which are based on the work of other people and thus have partly different license models:

    MicroMod Library by Martin Cameron (there's no license info in it and the page is down, but it was released under a proprietary OS license.
    GifEncoder Library by Jef Poskanzer released under a proprietary OS license (see source for details).

Each file of the source code should contain a header with the license information.